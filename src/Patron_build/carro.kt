package Patron_build

import Componentes.*

class carro (modelo: modelo,motor: motor, gps: gps, computadora: computadora, asientos: asientos) {


    constructor(carro2: carro) : this(carro2.model,carro2.mot,carro2.gp,carro2.compu,carro2.asient) {
    }

    var model = modelo
        get() = field
        set(value) {
            field = value
        }
    var mot = motor
        get() = field
        set(value) {
            field = value
        }
    var gp = gps
        get() = field
        set(value) {
            field = value
        }
    var compu = computadora
        get() = field
        set(value) {
            field = value
        }
    var asient = asientos
        get() = field
        set(value) {
            field = value
        }



}